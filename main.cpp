//IN THE NAME OF GOD
//Include Standard I/O Steam
#include <iostream>
//Include Standard String Library
#include <string>
//Include Vector Library
#include <vector>
//getch()
#include <conio.h>
//Include Standard File Stream Library
#include "IOFILE.h"
//Include Sleep, For Set Delay
#include <windows.h>
//Report Header
#include "Report.h"
//system Command
#include <stdlib.h>
//getchar
#include <stdio.h>

//About The Program
#define ATP_1 "Just for Educational Purposes"
#define ATP_2 "Writted in 2017 Jun 04"

//Define NameSpace For Compiler
using namespace FileWork;
//Use uint -> Unsigned Short Int
typedef unsigned short int uint;

//Structure For Book
struct Book{
    std::string name;
    std::string code;
};
//Structure For Student
struct Student{
    std::string name;
    std::string code;
};
//Structure For Reservation
struct Reserve{
    std::string s_code;
    std::string b_code;
};

//Function For Student register
void Student_Register (void);
//Function For Book register
void Book_Register    (void);
//Function For Reserve Book
void Book_Reserve     (void);
//Function For Report
void Report(uint);
//Revocation Reserve
void Revocation_Reserve(void);
//Menu
void Menu (uint&);

//Main Program
int main(void){
    //Create A Folder
    File::CreateDirector("DataBase");
    //Variable For While-Loop Control
    bool flag = true;
    //Keep user Selection
    uint selectet = 0;
    //Main Loop
    while(true){
        //Show Option
        Menu(selectet);
        //Switch
        switch(selectet){
        case 1:
            //if one,User Want Register a new Student
            Student_Register();
            break;
        case 2:{
            //if tow,User Want Edit a Student
                //Report All Registered Student
                Report(2);
                std::cout << "\n---------------------------------------------\n";
                std::string key;
                //Clear Keyboard Buffer
                getchar();
                std::cout << "Enter Code or Name >> ";
                std::getline(std::cin,key);
                //Edit Student
                report::Edit(key);
                //Book_Register();
            }
            break;
        case 3:
            //if three,User Want Register a new Book
            Book_Register();
            getch();
            break;
        case 4:{
            //if one,User Want Edit a Book
            //Report All Registered Book
            Report(1);
            std::cout << "\n---------------------------------------------\n";
            std::string key;
            getchar();
            std::cout << "Enter Code or Name >>";
            //Get Book Name From User
            std::getline(std::cin,key);
            //Edit Book
            report::Edit(key,false);
        }
            getch();
            break;
        case 5:
            //if five,User Want Reserve a Book
            Book_Reserve();
            getch();
            break;
        case 6:
            //if six,User Want Revocation a Reserve
            Revocation_Reserve();
            getch();
            break;
        case 7:
            //if seven,User Want a Report from Student
            Report(2);
            getch();
            break;
        case 8:
            //if eight,User Want a Report From Reserved
            Report(4);
            getch();
            break;
        case 9:
            //if nine,User Want a Report from Book
            Report(1);
            getch();
            break;
        case 10:
            exit(0);
            break;
        default:
            std::cout << "Wrong Input.!";
            getch();
            break;
        }
    }
    //End Program
    return 0;
}

//Start Function
void Student_Register(void){
    //String Variable For Keep Name and Code
    Student R_1;
    //Get Information From User
    RETURN:
    //Create a Restore Point
    getchar();
    //Clear Keyboard Buffer
    std::cout << "Enter Student Name >> ";
    std::getline(std::cin,R_1.name);
    if(!report::Search(R_1.name)) {std::cout << "This Name Exist."; goto RETURN;}
    //Check the Entered Name Don't Exist
    std::cout << "Enter Student Code >> ";
    std::getline(std::cin,R_1.code);
    if(!report::Search(R_1.code)) {std::cout << "This Code Exist."; goto RETURN;}
    //Check the Entered Code Don't Exist
    //Waiting
    std::cout << "Wait...\n";
    //Function to Set Delay
    Sleep(500);
    //Write in to File
    File::WriteLine(ADD_STU,R_1.name+"-"+R_1.code+"\n",true);
    std::cout << "Operation Well Down\nPress Any Key to Continue...";
    getch();
}
//END Function

//START Function
void Book_Register(void){
    //Variable For Keep Name and Code
    Book R_2;
    //Get Information From User
    RETURN :
    //Create a Restore Point
    getchar();
    //Clear Keyboard Buffer
    std::cout << "Enter Book Name >>";
    std::getline(std::cin,R_2.name);
    if(!report::Search(R_2.name)) {std::cout << "This Name Exist."; goto RETURN;}
    //Check the Entered Name Don't Exist
    std::cout << "Enter Book Code >>";
    std::getline(std::cin,R_2.code);
    if(!report::Search(R_2.code)) {std::cout << "This Code Exist."; goto RETURN;}
    //Check the Entered Code Don't Exist
    //Waiting
    std::cout << "Wait...\n";
    Sleep(500);
    //Write in to File
    File::WriteLine(ADD_BOK,R_2.name+"-"+R_2.code+"\n",true);
    std::cout << "Operation Well Down\nPrees Any Key to Continue...";
}
//END Function

//START Function
void Book_Reserve(void){
    std::cout << "\t Registered Book \n";
    //Book Report
    Report(1);
    std::cout << "---------------------------------------------\n";
    std::cout << "\t Registered Student \n";
    //Student Report
    Report(2);
    //User Selection
    Reserve R_3;
    std::cout << "\n---------------------------------------------\n";
    //Clear Buffer
    getchar();
    std::cout << "Enter Student Code >> ";
    std::getline(std::cin,R_3.s_code);
    std::cout << "Enter Book    Code >> ";
    std::getline(std::cin,R_3.b_code);
    //End Information

    //Write in to File
    File::WriteLine(ADD_RES,R_3.s_code+"-"+R_3.b_code+"\n",true);
}
//END Function

//START Function
void Revocation_Reserve(void){
    Reserve R_4;
    //Reserved Report
    Report(4);
    std::cout << "\n---------------------------------------------\n";
    getchar();
    //Clear Keyboard Buffer
    std::cout << "Enter Student Code >> ";
    std::getline(std::cin,R_4.s_code);
    std::cout << "Enter Book    Code >> ";
    std::getline(std::cin,R_4.b_code);
    //End Information

    std::vector <std::string> Liner;
    File::ReadLine(ADD_RES,Liner);
    //Read Each Line in File, Store in Vector <Liner>
    uint pos=0;
    //For Keep Position
    for(uint i=0;i != Liner.size(); i++){
        //Use .find(), for Search a String in Another String
        if(Liner[i].find(R_4.s_code)!=std::string::npos
        && Liner[i].find(R_4.b_code)!=std::string::npos){
            //if Find, Position store in pos <uint>
            pos = i;
            break;
        }
    }
    //First Remove File
    File::RemoveFile(ADD_RES);
    //Then Write again data in File Without Line -> pos
    for(uint i=0;i != Liner.size(); i++){
        if(i != pos)
            File::WriteLine(ADD_RES,Liner[i]+"\n",true);
    }
}
//END Function

//START Function
void Report(uint choose){
    switch(choose){
    case 1:
        report::Book_Report();
        break;
    case 2:
        report::Student_Report();
        break;
    case 4:
        report::Reserv_Report();
        break;
    default:
        std::cout << "Wrong Input~!";
        break;
    }
}
//END Function

//START function
void Menu(uint &re){
    system("cls");
    std::cout << "\n\n\n\n\n";
    std::cout << "\t\t<1>-Student Register\n";
    std::cout << "\t\t<2>-Student Edit\n";
    std::cout << "\t\t<3>-Book Register\n";
    std::cout << "\t\t<4>-Book Edit\n";
    std::cout << "\t\t<5>-Reserved Book\n";
    std::cout << "\t\t<6>-Revocation Reserve\n";
    std::cout << "\t\t<7>-Full Report Student\n";
    std::cout << "\t\t<8>-Full Report Reserved Book\n";
    std::cout << "\t\t<9>-Full Report Book\n";
    std::cout << "\t\t<10>-Exit\n\t\t>>";
    std::cin  >>re;
}
//END function
