//Define Gourd
#ifndef IOFILE_H
#define IOFILE_H
//Standard IO Library
#include<iostream>
//Standard String Library
#include<string>
//C Library
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
//File Stream
#include<sstream>
#include<fstream>
//Standard Vector Library
#include<vector>
//For Control Director
#include<dir.h>
//Define File Address
#define ADD_STU "DataBase\\stu.txt"
    //Student
#define ADD_BOK "DataBase\\bok.txt"
    //Book
#define ADD_RES "DataBase\\res.txt"
    //Reserve
#define ADD_ORE "DataBase\\ore.txt"
namespace FileWork{
    class File{
    public:
        //Function For Write a String in File
        //1 -> File Address
        //2 -> Value
        //3 -> Append to End of File
        static int WriteLine(const std::string Address,std::string _Value,bool AppendMode=false){
            std::ofstream W_L;
            if(AppendMode==true)
                W_L.open(Address.c_str(),std::ios_base::app);
            else
                W_L.open(Address.c_str());
            if(W_L.is_open()==true){
                W_L<<_Value;
                W_L.close();
                return 0;
            }
            else
                return 1;

        }//END
        //Function For Remove a File
        //1 -> File Address
        static int RemoveFile(const std::string Address){
            int _R_=remove(Address.c_str());
            if(_R_!=0){
                perror("<ERROR>");
                return 1;
            }//END if
            else{
                puts("File Successfully Deleted");
                return 0;
            }//END else
        }//END
        //Function For Rename File
        //1 -> File Address
        //2 -> New Name
        //static int RenameFile(const std::string Address,std::string NewName){
        //    int _R_=rename(Address.c_str(),NewName.c_str());
        //    if(_R_==0){
        //        puts("File Successfully Renamed");
        //        return 0;
        //    }//END if
        //    else{
        //        perror("<ERROR>");
        //        return 1;
        //    }//END else
        //}
        //Function For Read File data
        //1 -> File Address
        //2 -> Read With Space or Read Without Space
        //3 <- Return String
        //static std::string ReadFile(const std::string Address,bool NoSpace=false){
        //    std::ifstream R_F(Address.c_str());
        //    std::string Result;
        //    std::string Returned;
        //    if(R_F.is_open()==true){
        //        while(!R_F.eof()){
        //            if(NoSpace==false)
        //                std::getline(R_F,Result);
        //            else{
        //                R_F>>Result;
        //                Returned+=Result;
        //            }//END if
        //        }//END while
        //    }//END if
        //    if(NoSpace==false)
        //        return Result;
        //    else
        //        return Returned;

        //}//END

        //Function For Read File Line by Line
        //1 -> File Address
        //2 -> vector For Keep Each Line
        static void ReadLine(const std::string Address,std::vector <std::string> &LinebyLine){
            std::ifstream R_F(Address.c_str());
            std::string Result;
            if(R_F.is_open()==true)
                for( std::string line;std::getline(R_F,line);)
                    //Read Each Line Form File and Store in Vector
                    LinebyLine.push_back(line);
        }//END
        //Function For Create a New Directory
        //1 -> Directory Name
        static void CreateDirector(std::string name){
        	mkdir(name.c_str());
        }//END

    };//END class
}//END Name Space
#endif
