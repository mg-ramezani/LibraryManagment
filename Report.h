//END
#ifndef REPORT_H_INCLUDED
#define REPORT_H_INCLUDED
//Include Tools For Use File
#include"IOFILE.h"
//Use uint -> Unsigned Short Int
typedef unsigned short int uint;
//Define NameSpace For Compiler
using namespace FileWork;
//Start Class
class report{
public:
    //Function For Report From Book
    static void Book_Report(void){
        //std::cout << File::ReadFile(ADD_BOK);
        //Read All Line in bok.txt,
        //and Print Each Line
        std::vector <std::string> Book_Liner;
        //Read Each Line
        File::ReadLine(ADD_BOK,Book_Liner);
        for(uint i=0; i != Book_Liner.size();i++)
            std::cout << Book_Liner[i] << std::endl;
    }
    //Function For Report From Student
    static void Student_Report(void){
        //std::cout << File::ReadFile(ADD_STU);
        //Read All Line in stu.txt,
        //and Print Each Line
        std::vector <std::string> Student_Liner;
        //Read Each Line
        File::ReadLine(ADD_STU,Student_Liner);
        for(uint i=0; i != Student_Liner.size();i++)
            std::cout << Student_Liner[i] << std::endl;
    }
    //Function For Report From Reserved
    static void Reserv_Report(void){
        //Because only the code is stored in the file.
        //Read the codes and print the names of people
        std::vector <std::string> Reserv_Liner;
        std::vector <std::string> Book_Liner;
        std::vector <std::string> Student_Liner;
        //Read Each Line in res.txt
        File::ReadLine(ADD_RES,Reserv_Liner);
        //Read Each Line in bok.txt
        File::ReadLine(ADD_BOK,Book_Liner);
        //Read Each Line in stu.txt
        File::ReadLine(ADD_STU,Student_Liner);
        int split;
        std::string parsing;
        for(uint i=0;i != Reserv_Liner.size();i++){
                split = Reserv_Liner[i].find("-");
                //Find Position of '-'
                parsing = Reserv_Liner[i].substr(0,split);
                //Read all Char(Student Code) Before '-'
                for(uint i=0; i!=Student_Liner.size();i++){
                    //Search Student Code in Registered Student
                    if(Student_Liner[i].find(parsing)!=std::string::npos){
                        //if Find Print it
                        std::cout << Student_Liner[i] << "-->";
                        break;
                    }//END if
                }//END for
                parsing = Reserv_Liner[i].substr(split+1,Reserv_Liner[i].length());
                //Read all Char(Book Code) After '-'
                for(uint i=0; i!=Book_Liner.size();i++){
                    //Search Book Code in Registered Book
                    if(Book_Liner[i].find(parsing)!=std::string::npos){
                        //if Find Print it
                        std::cout << Book_Liner[i] << "\n";
                        break;
                    }//END if
                }//END for
        }//END for
    }//END
    //Function For Edit Student or Book
    //if mode -> true => Only Edit Student
    //if mode -> false=> Only Edit Book
    static void Edit(std::string key,bool mode=true){
        std::vector <std::string> Liner;
        //Vector for Save new Value in File
        std::string name,code;
        //name -> New Name
        //code -> New Code
        if(mode == true)
            //if true -> open stu.txt
            File::ReadLine(ADD_STU,Liner);
        else
            //if false-> open bok.txt
            File::ReadLine(ADD_BOK,Liner);
        int pos=-1;
        //pos = -1 => not Find
        for(uint i=0;i != Liner.size(); i++){
            //Read Line by Line
            if(Liner[i].find(key)!=std::string::npos){
                //if Find, pos =i[Position]
                pos = i;
                break;
            }//END if
            pos = -1;
            //if not Find, pos = -1
        }//END for
        if(pos != -1){
            RETURN:
            //Create Restore Point
            std::cout << "Enter New Name >> ";
            std::getline(std::cin,name);
            //get new Name From User
            if(!report::Search(name)) {std::cout << "This Name Exist."; getchar(); goto RETURN;}
            //Check, the Name don't Exits
            std::cout << "Enter New Code >> ";
            std::getline(std::cin,code);
            if(!report::Search(code)) {std::cout << "This Code Exist."; goto RETURN;}

            //Remove Old File
            if(mode == true)
                File::RemoveFile(ADD_STU);
            else
                File::RemoveFile(ADD_BOK);
            //Create New File and
            //Write Value in File
            for(uint i=0;i != Liner.size(); i++){
                if(i != pos){
                    if(mode == true)
                        File::WriteLine(ADD_STU,Liner[i]+"\n",true);
                    else
                        File::WriteLine(ADD_BOK,Liner[i]+"\n",true);
                }//END if
                else{
                    if(mode == true)
                        File::WriteLine(ADD_STU,name+"-"+code+"\n",true);
                    else
                        File::WriteLine(ADD_BOK,name+"-"+code+"\n",true);
                }//END else
            }//END for
        }//END if
        else{
            std::cout << "Not Find";
            getch();
        }//END else
    }//END
    //Function For Search
    //if key find in bok.txt or stu.txt
    //Return true
    static bool Search(std::string key){

        std::vector <std::string> Book_Liner;
        std::vector <std::string> Student_Liner;
        File::ReadLine(ADD_BOK,Book_Liner);
        File::ReadLine(ADD_STU,Student_Liner);
        //Search in bok.txt
        for(uint i=0; i!=Book_Liner.size();i++){
            if(Book_Liner[i].find(key)!=std::string::npos)
                return false;
        }//END for
        //Search in stu.txt
        for(uint i=0; i!=Student_Liner.size();i++){
            if(Student_Liner[i].find(key)!=std::string::npos)
                return false;
        }//END for
        return true;
    }//END
};//END class

#endif // REPORT_H_INCLUDED
